MAVEN_VERSION=$(mvn -q -Dexec.executable=echo -Dexec.args='${project.version}' --non-recursive exec:exec)
MAVEN_VERSION=${MAVEN_VERSION%'-SNAPSHOT'}
TIMESTAMP=$(date "+%Y%m%d.%H%M%S")
VERSION="${MAVEN_VERSION}-${TIMESTAMP}-${BUILD_NUMBER}"
mvn versions:set -DnewVersion=${VERSION}
mvn clean deploy -Pimage -Ddocker.nexus.password=admin123
